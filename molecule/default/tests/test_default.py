import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('xrdp')


def test_xrdp(host):
    service = host.service("xrdp")
    assert service.is_running
    assert service.is_enabled
